#!/usr/bin/env sh

################################################################################
# Fix possible problem with ALL_PROXY and Docker/Go
export ALL_PROXY=""
export all_proxy=""

################################################################################
# Check failure conditions
cd $(dirname $0)/definitions || exit 1
if [ -z "$HTTP_PROXY" ]
then
    echo "No HTTP_PROXY"
    exit 1
elif [ -z "$CI_COMMIT_REF_NAME" ]
then
    echo "No CI_COMMIT_REF_NAME"
    exit 1
fi

################################################################################
# Properly set proxy variables
for v in http_proxy FTP_PROXY ftp_proxy ALL_PROXY all_proxy HTTPS_PROXY https_proxy HTTP_PROXY
do
    proxy="$(eval "echo \$$v")"
    if [ -z "$proxy" ]
    then
        export $v="$HTTP_PROXY"
    fi
done

################################################################################
# Set default values
if [ -z "$TAG_PREFIX" ]
then
    TAG_PREFIX=forge.univ-lyon1.fr:4567/forge_public/ci-images
    echo "No TAG_PREFIX, defaulting to $TAG_PREFIX"
fi

status=0

for def in *
do
    (cd $def
     echo "Building $def ..."
     if [ -f tags -a -x run-template.sh ]
     then
         for t in $(cat tags)
         do
             image_tag=$TAG_PREFIX/$def:$t
             echo "Running $def:$t in $(pwd)"
             which sh
             sh run-template.sh $t
             (docker build --build-arg http_proxy=$HTTP_PROXY -t $image_tag . 2>&1 > dockerbuild.log || (cat dockerbuild.log; exit 1)) && (docker push $image_tag 2>&1 >> dockerbuild.log || (cat dockerbuild.log; exit 1)) && echo "$def:$t done" || exit 1
         done
     else
         image_tag=$TAG_PREFIX/$def:$CI_COMMIT_REF_NAME
         (docker build --build-arg http_proxy=$HTTP_PROXY -t $image_tag . 2>&1 > dockerbuild.log || (cat dockerbuild.log; exit 1)) && (docker push $image_tag 2>&1 >> dockerbuild.log || (cat dockerbuild.log; exit 1)) && echo "$def done" || exit 1
     fi
    ) || status=1
done

exit $status
