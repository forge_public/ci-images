#!/usr/bin/env bash
# Récupération de la liste des packages à installer
dnf list --installed | sed 's/\([^ ]*\) .*$/\1/' > packages.list
dnf repolist --enabled > repo.list

