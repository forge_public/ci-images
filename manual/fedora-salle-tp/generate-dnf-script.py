#!/usr/bin/env python3
PACKAGES_FILE="/tmp/packages.list"
EXCLUDE_FILE="/tmp/noinstall.list"
SCRIPT_FILE="/tmp/install-script.dnf"
INSTALL_LINE_TPL="install -y {}"

excluded = []
with open(EXCLUDE_FILE,'r') as f:
    for l in f:
        if '' != l.strip():
            excluded.append(l.strip())

with open(PACKAGES_FILE,'r') as f:
    with open(SCRIPT_FILE, 'w') as fw:
        for l in f:
            if l.strip() != '' and all(map(lambda e: (e not in l), excluded)):
                fw.write(INSTALL_LINE_TPL.format(l))

