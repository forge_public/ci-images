FROM maven:$TAG
ENV http_proxy=http://proxy.univ-lyon1.fr:3128 \
    FTP_PROXY=http://proxy.univ-lyon1.fr:3128\
    ftp_proxy=http://proxy.univ-lyon1.fr:3128 \
    ALL_PROXY=http://proxy.univ-lyon1.fr:3128 \
    all_proxy=http://proxy.univ-lyon1.fr:3128 \
    HTTPS_PROXY=http://proxy.univ-lyon1.fr:3128 \
    https_proxy=http://proxy.univ-lyon1.fr:3128 \
    HTTP_PROXY=http://proxy.univ-lyon1.fr:3128 \
    NO_PROXY="localhost,127.0.0.1,forge.univ-lyon1.fr,docker"
RUN mkdir -p /root/.m2
COPY maven/settings.xml /root/.m2/settings.xml
